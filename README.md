##php ^7.4.6, mysql ^5.7

## Installation

Clone the repository
    
    git clone https://munirhuja1357@bitbucket.org/munihuja1357/adminlte-product.git
    
Switch to the repo folder

    cd adminlte-product

Install all the dependencies using composer

    composer install

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Generate a new application key

    php artisan key:generate

Run the database migrations (**Set the database connection in .env before migrating**)

    php artisan migrate
    php artisan db:seed

Start the local development server

    php artisan serve

You can now access the server at http://localhost:8000

## command list
    git clone https://munirhuja1357@bitbucket.org/munihuja1357/adminlte-product.git
    cd adminlte-product
    composer install
    cp .env.example .env
    php artisan key:generate
    
**Make sure you set the correct database connection information before running the migrations** [Environment variables](#environment-variables)

    php artisan migrate
    php artisan db:seed
    php artisan serve

## Database seeding

**Populate the database with seed data with relationships which includes users, articles, comments, tags, favorites and follows. This can help you to quickly start testing the api or couple a frontend and start using it with ready content.**

Run the database seeder and you're done

    php artisan db:seed

***Note*** : It's recommended to have a clean database before seeding. You can refresh your migrations at any point to clean the database by running the following command

    php artisan migrate:refresh
    
##Test with DUSK

    php artisan dusk
# Code overview

## Dependencies

- [fruitcake/laravel-cors](https://github.com/fruitcake/laravel-cors) - For handling Cross-Origin Resource Sharing (CORS)
- [guzzlehttp/guzzle](https://github.com/guzzle/guzzle) - For makes it easy to send HTTP requests and trivial to integrate with web services
- [spatie/laravel-permission](https://github.com/spatie/laravel-permission) - For associate users with permissions and roles
- [yajra/laravel-datatables](https://github.com/yajra/laravel-datatables) - For jQuery DataTables API

## Folders

- `app` - Contains all the Eloquent models
- `app/Models` - Contains all the Eloquent models
- `app/Http/Controllers` - Contains all the api controllers
- `config` - Contains all the application configuration files
- `database/factories` - Contains the model factory for all the models
- `database/migrations` - Contains all the database migrations
- `database/seeds` - Contains the database seeder
- `routes` - Contains all routes defined in web.php file

## Environment variables

- `.env` - Environment variables can be set in this file


***Note*** : You can quickly set the database information and other variables in this file and have the application fully working.

----------
## License

Licensed under the [MIT License](http://opensource.org/licenses/MIT).

