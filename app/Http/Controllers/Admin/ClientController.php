<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\User;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return Datatables::of(Client::with('user'))
                ->editColumn('created_at', function ($data) {
                    return $data->created_at->format('d.m.Y h:m:s');
                })
                ->editColumn('updated_at', function ($data) {
                    return $data->updated_at->format('d.m.Y h:m:s');
                })
                ->addColumn('photo', function ($data) {
                    $img = '<img width="150px" src="' . asset($data->getFirstMediaUrl('avatars', 'small')) . '" />';
                    return $img;
                })
                ->addColumn('action', function ($data) {
                    $btns = '<a class="btn btn-sm btn-primary" href="' . route('admin.clients.edit', $data->id) . '">Редактировать</a>';
                    $btns .= '<button class="btn btn-sm btn-danger" onclick="Delete(' . $data->id . ')" >Удалить</button>';
                    return $btns;
                })
                ->rawColumns(['action', 'photo'])
                ->make(true);;
        }
        return view('admin.client.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.client.create');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'full_name' => 'required|string|min:2|max:256',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|confirmed|min:6|max:30',
            'photo' => 'nullable|mimes:jpg,png|max:5120|dimensions:min_width=300,min_height=300',
            'phone' => ['required', 'regex:/^(\+\d{1,2}\s?)?1?\-?\.?\s?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/', 'unique:clients,phone_number', 'min:10', 'max:20']
        ]);
        $user = new User();
        $user->name = $request->input('full_name');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->save();
        $user->assignRole('client');
        $client = new Client();
        $client->full_name = $request->input('full_name');
        $client->phone_number = $request->input('phone');
        $client->user_id = $user->id;
        $client->save();
        $client->addMedia($request->file('photo'))->toMediaCollection('avatars');
        return redirect(route('admin.clients.index'))->with(['success' => 'Данные добавлены']);
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.client.edit')->with(['client' => Client::with('user')->findOrFail($id)]);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_id = Client::findOrFail($id)->user_id;
        $this->validate($request, [
            'full_name' => 'required|string|min:2|max:256',
            'email' => 'required|email|unique:users,email,' . $user_id,
            'password' => 'nullable|confirmed|min:6|max:30',
            'photo' => 'nullable|mimes:jpg,png|max:5120|dimensions:min_width=300,min_height=300',
            'phone' => ['required', 'regex:/^(\+\d{1,2}\s?)?1?\-?\.?\s?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/', 'unique:clients,phone_number,' . $id]
        ]);
        $user = User::findOrFail($user_id);
        $user->name = $request->input('full_name');
        $user->email = $request->input('email');
        if ($request->has('password')) {
            $user->password = bcrypt($request->input('password'));
        }
        $user->save();
        $client = Client::find($id);
        $client->full_name = $request->input('full_name');
        $client->phone_number = $request->input('phone');
        $client->save();
        if ($request->has('photo')) {
            $client->clearMediaCollection('avatars');
            $client->addMedia($request->file('photo'))->toMediaCollection('avatars');
        }
        return redirect(route('admin.clients.index'))->with(['success' => 'Данные обновлены']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = Client::findOrFail($id);
        $client->clearMediaCollection('avatars');
        return Response()->json($client->delete());
    }
}
