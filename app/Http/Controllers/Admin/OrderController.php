<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return Datatables::of(Order::with(['client', 'product']))
                ->editColumn('created_at', function ($data) {
                    return $data->created_at->format('d.m.Y h:m:s');
                })
                ->editColumn('updated_at', function ($data) {
                    return $data->updated_at->format('d.m.Y h:m:s');
                })
                ->addColumn('photo', function ($data) {
                    $img = '<img width="150px" src="' . asset($data->product->getFirstMediaUrl('photo', 'small')) . '" />';
                    return $img;
                })
                ->rawColumns(['photo'])
                ->make(true);;
        }
        return view('admin.order.index');

    }
    //
}
