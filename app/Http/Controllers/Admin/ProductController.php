<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {
            return Datatables::of(Product::all())
                ->editColumn('created_at', function ($data) {
                    return $data->created_at->format('d.m.Y h:m:s');
                })
                ->editColumn('updated_at', function ($data) {
                    return $data->updated_at->format('d.m.Y h:m:s');
                })
                ->addColumn('photo', function ($data) {
                    $img = '<img width="150px" src="' . asset($data->getFirstMediaUrl('photo', 'small')) . '" />';
                    return $img;
                })
                ->addColumn('action', function ($data) {
                    $btns = '<a class="btn btn-sm btn-primary" href="' . route('admin.products.edit', $data->id) . '">Редактировать</a>';
                    $btns .= '<button class="btn btn-sm btn-danger" onclick="Delete(' . $data->id . ')" >Удалить</button>';
                    return $btns;
                })
                ->rawColumns(['action', 'photo'])
                ->make(true);;
        }
        return view('admin.product.index');
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.product.create');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|min:2|max:256',
            'sku' => 'required|string|min:2|max:256|unique:products,sku',
            'price' => 'required|numeric|min:0.01',
            'photo' => 'nullable|mimes:jpg,png|max:5120|dimensions:min_width=300,min_height=300'
        ]);
        $product = new Product();
        $product->name = $request->input('name');
        $product->sku = $request->input('sku');
        $product->price = $request->input('price');
        $product->save();
        $product->addMedia($request->file('photo'))->toMediaCollection('photo');
        return redirect(route('admin.products.index'))->with(['success' => 'Данные добавлены']);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.product.edit')->with(['product' => Product::findOrFail($id)]);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|min:2|max:256',
            'sku' => 'required|string|min:2|max:256|unique:products,sku,' . $id,
            'price' => 'required|numeric|min:0.01',
            'photo' => 'nullable|mimes:jpg,png|max:5120|dimensions:min_width=300,min_height=300'
        ]);
        $product = Product::findOrFail($id);
        $product->name = $request->input('name');
        $product->sku = $request->input('sku');
        $product->price = $request->input('price');
        $product->save();
        if ($request->has('photo')) {
            $product->clearMediaCollection('photo');
            $product->addMedia($request->file('photo'))->toMediaCollection('photo');
        }
        return redirect(route('admin.products.index'))->with(['success' => 'Данные добавлены']);
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->clearMediaCollection('photo');
        return Response()->json($product->delete());
    }
}
