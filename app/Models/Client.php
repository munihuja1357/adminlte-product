<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Client extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    protected $fillable = ['user_id', 'full_name', 'phone_number','admin_created_at','admin_updated_at'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function orders(){
        return $this->hasMany(Order::class);
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('avatars')
            ->useFallbackUrl('/img/avatar.jpeg')
            ->useFallbackPath(public_path('/img/avatar.jpeg'));
    }


    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('small')
            ->crop('crop-center',300,300)
            ->format('jpg')
            ->quality(80)
            ->sharpen(10)
            ->performOnCollections('avatars');
    }
}
