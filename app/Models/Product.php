<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Product extends Model  implements HasMedia
{
    use HasFactory,InteractsWithMedia;

    protected $fillable = [ 'name', 'SKU','price','admin_created_at','admin_updated_at'];

    public function orders(){
        return $this->hasMany(Order::class);
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('photo')
            ->useFallbackUrl('/img/63f2afb6927676e79a0c9d48a69f52a2.png')
            ->useFallbackPath(public_path('/img/63f2afb6927676e79a0c9d48a69f52a2.png'));
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('small')
            ->crop('crop-center',300,300)
            ->format('jpg')
            ->quality(80)
            ->sharpen(10)
            ->performOnCollections('photo');
    }
}
