<?php

namespace App\Observers;

use App\Models\Client;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ClientObserver
{
    /**
     * Handle the Client "created" event.
     *
     * @param \App\Models\Client $client
     * @return void
     */
    public function created(Client $client)
    {
        if (Auth::check() && Auth::user()->hasRole('admin')) {
            $client->admin_created_at = Carbon::now();
        }
        //
    }

    /**
     * Handle the Client "updated" event.
     *
     * @param \App\Models\Client $client
     * @return void
     */
    public function updated(Client $client)
    {
        if (Auth::check() && Auth::user()->hasRole('admin')) {
            $client->admin_updated_at = Carbon::now();
        }
        //
    }

    /**
     * Handle the Client "deleted" event.
     *
     * @param \App\Models\Client $client
     * @return void
     */
    public function deleted(Client $client)
    {
        //
    }

    /**
     * Handle the Client "restored" event.
     *
     * @param \App\Models\Client $client
     * @return void
     */
    public function restored(Client $client)
    {
        //
    }

    /**
     * Handle the Client "force deleted" event.
     *
     * @param \App\Models\Client $client
     * @return void
     */
    public function forceDeleted(Client $client)
    {
        //
    }
}
