<?php

namespace Database\Factories;

use App\Models\Client;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ClientFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Client::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'full_name' => $this->faker->name,
            'phone_number' => $this->faker->unique()->phoneNumber,
            'created_at' => \Carbon\Carbon::createFromTimeStamp($this->faker->dateTimeBetween('-100 days', 'now')->getTimestamp()),
            'updated_at' => \Carbon\Carbon::createFromTimeStamp($this->faker->dateTimeBetween('-100 days', 'now')->getTimestamp()),
            'user_id' =>  \App\Models\User::factory(),
        ];
    }
}
