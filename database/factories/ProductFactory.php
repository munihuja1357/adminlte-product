<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
            'SKU'  =>$this->faker->unique()->randomNumber(9),
            'price' =>$this->faker->randomFloat('2',0.1,1000000),
            'created_at' => \Carbon\Carbon::createFromTimeStamp($this->faker->dateTimeBetween('-100 days', 'now')->getTimestamp()),
            'updated_at' => \Carbon\Carbon::createFromTimeStamp($this->faker->dateTimeBetween('-100 days', 'now')->getTimestamp()),
            //
        ];
    }
}
