<?php

namespace Database\Seeders;

use App\Models\Client;
use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::create(['name' => 'add client']);
        Permission::create(['name' => 'edit client']);
        Permission::create(['name' => 'delete client']);
        Permission::create(['name' => 'view product']);
        Permission::create(['name' => 'add product']);
        Permission::create(['name' => 'edit product']);
        Permission::create(['name' => 'delete product']);

        $role = Role::create(['name' => 'client']);
        $role->givePermissionTo('view product');

        $role = Role::create(['name' => 'admin']);
        $role->givePermissionTo(Permission::all());

        $user= new User();
        $user->name='Administrator';
        $user->email='admin@admin.com';
        $user->password=bcrypt('password');
        $user->save();

        $user->assignRole('admin');

        \App\Models\Client::factory()->count(2100)->create();
        \App\Models\Product::factory()->count(3100)->create();
        $faker = \Faker\Factory::create();
        for ($i=1;$i<=3000;$i++) {
            $client_id=rand(1,2100);
            $created=\Carbon\Carbon::createFromTimeStamp($faker->dateTimeBetween('-100 days', 'now')->getTimestamp());
            $updated=\Carbon\Carbon::createFromTimeStamp($faker->dateTimeBetween('-100 days', 'now')->getTimestamp());
            for ($j = 1; $j <= rand(1, 6); $j++){
                Order::create([
                    'order_group'=>$i,
                    'client_id'=>$client_id,
                    'product_id'=>rand(1,3100),
                    'created_at'=>$created,
                    'updated_at'=>$updated
                ]);
            }
        }
    }
}
