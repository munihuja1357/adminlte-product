@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Clients-Add new client</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('admin.clients.store')  }}" method="POST" enctype="multipart/form-data">

                        <div class="form-body col-md-12">
                            @csrf
                            <div class="form-group col-md-6 float-md-left">
                                <label class="control-label">
                                    ФИО
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <input type="text"
                                       class="form-control  {{ $errors->has('full_name')?' is-invalid':'' }}"
                                       name="full_name" id="full_name" required min="2" max="256"
                                       value="{{old('full_name')}}">
                                @if($errors->has('full_name'))
                                    <div class='col-sm-12'><small class='text-danger'>
                                            @foreach($errors->get('full_name') as $message)
                                            {{ $message }}</br>
                                            @endforeach
                                        </small></div>
                                @endif
                            </div>
                            <div class="form-group col-md-6 float-md-right">
                                <label class="control-label">
                                    Э-почта
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <input type="email" class="form-control  {{ $errors->has('email')?' is-invalid':'' }}"
                                       name="email" id="email" required min="4" max="256" value="{{old('email')}}">
                                @if($errors->has('email'))
                                    <div class='col-sm-12'><small class='text-danger'>
                                            @foreach($errors->get('email') as $message)
                                            {{ $message }}</br>
                                            @endforeach
                                        </small></div>
                                @endif
                            </div>

                            <div class="form-group col-md-6 float-md-left">
                                <label class="control-label">
                                    Телефон
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <input type="text" class="form-control  {{ $errors->has('phone')?' is-invalid':'' }}"
                                       name="phone" id="phone" required min="10" max="20" value="{{old('phone')}}">
                                @if($errors->has('phone'))
                                    <div class='col-sm-12'><small class='text-danger'>
                                            @foreach($errors->get('phone') as $message)
                                            {{ $message }}</br>
                                            @endforeach
                                        </small></div>
                                @endif
                            </div>
                            <div class="form-group col-md-6 float-md-right">
                                <label class="control-label">
                                    Фотография
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <input type="file" class="form-control  {{ $errors->has('photo')?' is-invalid':'' }}"
                                       name="photo" id="photo">
                                @if($errors->has('photo'))
                                    <div class='col-sm-12'><small class='text-danger'>
                                            @foreach($errors->get('photo') as $message)
                                            {{ $message }}</br>
                                            @endforeach
                                        </small></div>
                                @endif
                            </div>

                            <div class="form-group col-md-6 float-md-left">
                                <label class="control-label">
                                    Пароль
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <input type="password"
                                       class="form-control  {{ $errors->has('password')?' is-invalid':'' }}"
                                       name="password" id="password" required min="6" max="30">
                                @if($errors->has('password'))
                                    <div class='col-sm-12'><small class='text-danger'>
                                            @foreach($errors->get('password') as $message)
                                            {{ $message }}</br>
                                            @endforeach
                                        </small></div>
                                @endif
                            </div>
                            <div class="form-group col-md-6 float-md-right">
                                <label class="control-label">
                                    Потвердить пароль
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <input type="password" class="form-control"
                                       name="password_confirmation" id="password_confirmation" required min="6"
                                       max="30">
                            </div>
                        </div>
                        <div class="row col-md-12">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-circle btn-primary">
                                    Сохранить
                                </button>
                                <a class="btn btn-circle btn-danger" href="{{ route('admin.clients.index') }}">
                                    Отмена
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script>
    </script>
@stop
