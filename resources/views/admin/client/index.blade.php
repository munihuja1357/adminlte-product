@extends('adminlte::page')

@section('title', 'AdminLTE')
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
@stop

@section('content_header')
    <h1 class="m-0 text-dark">Clients</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    @if (session('success'))
                        <div class="col-xs-12" style="padding-top: 20px;">
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <i class="icon fa fa-check"></i>
                                {{ session('success') }}
                            </div>
                        </div>
                @endif
                <!-- BEGIN Portlet PORTLET-->
                    <div class="portlet light" data-error-display="notific8">
                        <div class="portlet-title">
                            <div class="caption font-primary">
                            </div>
                            <div class="col-lg-9">
                                <a class="btn btn-sm btn-success" href="{{route('admin.clients.create')}}">
                                    <i class="fa fa-plus"></i>Добавить
                                </a>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-bordered" id="dataTable">
                                <thead>
                                <tr>
                                    <th>
                                        id
                                    </th>
                                    <th>
                                        ФИО
                                    </th>
                                    <th>
                                        Э-почта
                                    </th>
                                    <th>
                                        Номер телефона
                                    </th>
                                    <th>
                                        Аватар
                                    </th>
                                    <th>
                                        Дата создание
                                    </th>
                                    <th>
                                        Дата обновление
                                    </th>
                                    <th>
                                        Действие
                                    </th>
                                </tr>
                                </thead>

                            </table>
                        </div>

                    </div>
                    <!-- END PORTLET-->
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
    <script src="{{ asset('vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
    <script>
        $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: window.location.href,
            columns: [
                {data: 'id', name: 'id'},
                {data: 'full_name', name: 'full_name'},
                {data: 'user.email', name: 'email'},
                {data: 'phone_number', name: 'phone_number'},
                {data: 'photo', name: 'photo'},
                {data: 'created_at', name: 'created_at'},
                {data: 'updated_at', name: 'updated_at'},
                {data: 'action', name: 'action'}
            ]
        });

        function Delete(id) {
            swal({
                title: "Вы точно хотите удалить?",
                text: "Выбранные вами данные будут удалены",
                type: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type: 'DELETE',
                        dataType: 'json',
                        url: window.location.href + '/' + id,
                        data: ({"_token": $('meta[name="csrf-token"]').attr('content')})
                    }).done(function (success) {
                        if (success) {
                            $('#dataTable').DataTable().ajax.reload();
                            swal({
                                title: "Успешно удален!",
                                type: "success",
                            });
                        }
                    })

                }
            });
        }
    </script>
@stop
