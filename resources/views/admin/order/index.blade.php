@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Orders</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <!-- BEGIN Portlet PORTLET-->
                    <div class="portlet light" data-error-display="notific8">

                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-bordered" id="dataTable">
                                <thead>
                                <tr>
                                    <th>
                                        id
                                    </th>
                                    <th>
                                        ФИО
                                    </th>
                                    <th>
                                        Номер телефона
                                    </th>
                                    <th>
                                        Наименование товара
                                    </th>
                                    <th>
                                        SKU
                                    </th>
                                    <th>
                                        Цена
                                    </th>
                                    <th>
                                        Фотография товара
                                    </th>
                                    <th>
                                        Дата покупки
                                    </th>
                                    <th>
                                        Дата обновление
                                    </th>
                                </tr>
                                </thead>

                            </table>
                        </div>

                    </div>
                    <!-- END PORTLET-->
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="{{ asset('vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
    <script>
        $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: window.location.href,
            columns: [
                {data: 'id', name: 'id'},
                {data: 'client.full_name', name: 'full_name'},
                {data: 'client.phone_number', name: 'phone_number'},
                {data: 'product.name', name: 'name'},
                {data: 'product.sku', name: 'sku'},
                {data: 'product.price', name: 'price'},
                {data: 'photo', name: 'photo'},
                {data: 'created_at', name: 'created_at'},
                {data: 'updated_at', name: 'updated_at'}
            ]
        });
    </script>
@stop
