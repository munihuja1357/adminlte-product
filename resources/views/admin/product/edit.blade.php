@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Product-Edit</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('admin.products.update',$product->id)  }}" method="POST"
                          enctype="multipart/form-data">

                        <div class="form-body col-md-12">
                            @csrf
                            @method('put')
                            <div class="form-group col-md-6">
                                <label class="control-label">
                                    Наименование
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <input type="text"
                                       class="form-control  {{ $errors->has('name')?' is-invalid':'' }}"
                                       name="name" id="name" required min="2" max="256"
                                       value="{{$product->name}}">
                                @if($errors->has('name'))
                                    <div class='col-sm-12'><small class='text-danger'>
                                            @foreach($errors->get('name') as $message)
                                            {{ $message }}</br>
                                            @endforeach
                                        </small></div>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label">
                                    SKU
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <input type="text" class="form-control  {{ $errors->has('sku')?' is-invalid':'' }}"
                                       name="sku" id="sku" required min="4" max="256" value="{{$product->sku}}">
                                @if($errors->has('sku'))
                                    <div class='col-sm-12'><small class='text-danger'>
                                            @foreach($errors->get('sku') as $message)
                                            {{ $message }}</br>
                                            @endforeach
                                        </small></div>
                                @endif
                            </div>

                            <div class="form-group col-md-6">
                                <label class="control-label">
                                    Цена
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <input type="number" class="form-control  {{ $errors->has('price')?' is-invalid':'' }}"
                                       name="price" id="price" required min="0.01" value="{{$product->price}}"
                                       step="0.01">
                                @if($errors->has('price'))
                                    <div class='col-sm-12'><small class='text-danger'>
                                            @foreach($errors->get('price') as $message)
                                            {{ $message }}</br>
                                            @endforeach
                                        </small></div>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label">
                                    Фотография
                                </label>
                                <img width="300px" src="{{asset($product->getFirstMediaUrl('photo'))}}"/>
                                <input type="file" class="form-control  {{ $errors->has('photo')?' is-invalid':'' }}"
                                       name="photo" id="photo">
                                @if($errors->has('photo'))
                                    <div class='col-sm-12'><small class='text-danger'>
                                            @foreach($errors->get('photo') as $message)
                                            {{ $message }}</br>
                                            @endforeach
                                        </small></div>
                                @endif
                            </div>
                        </div>
                        <div class="row col-md-12">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-circle btn-primary">
                                    Сохранить
                                </button>
                                <a class="btn btn-circle btn-danger" href="{{ route('admin.products.index') }}">
                                    Отмена
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
