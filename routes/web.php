<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes([
    'register' => false, // Registration Routes...
    'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...
]);

Route::get('/home', [App\Http\Controllers\Admin\HomeController::class, 'index'])->name('home');

Route::group([
    'as' => 'admin.',
    'prefix' => 'admin/',
    'middleware' => [
        'auth',
        \App\Http\Middleware\CheckIsAdmin::class,
    ],
], function () {
    Route::resource('products', \App\Http\Controllers\Admin\ProductController::class, [
        'except' => [ 'show' ]
    ]);

    Route::resource('clients', \App\Http\Controllers\Admin\ClientController::class, [
        'except' => [ 'show' ]
    ]);
    Route::get('orders',[\App\Http\Controllers\Admin\OrderController::class,'index'])->name('orders');
});
