<?php

namespace Tests\Browser;

use App\Models\Client;
use App\Models\User;
use Database\Factories\UserFactory;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class AdminClientTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function ($index_visit,$create_visit,$store_visit) {
            $authUser=User::find(1);
            $index_visit->loginAs($authUser)
                ->visit('/admin/clients')
                ->assertSee('Clients');
            $create_visit->loginAs($authUser)
                ->visit('/admin/clients/create')
                ->assertSee('Clients-Add new client');
            $user=User::factory()->create();
            $client=Client::factory()->create();
            $store_visit->loginAs($authUser)
                ->visitRoute('admin.clients.create')
                ->type('email',$user->email)
                ->type('full_name',$client->full_name)
                ->type('phone',$client->phone_number)
                ->type('password','password')
                ->type('password_confirmation','password')
                ->press('Сохранить')
                ->assertSee('Потвердить пароль');
        });
    }
}
