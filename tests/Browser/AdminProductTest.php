<?php

namespace Tests\Browser;

use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class AdminProductTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function ($index_visit,$create_visit,$store_visit) {
            $authUser=User::find(1);
            $index_visit->loginAs($authUser)
                ->visit('/admin/products')
                ->assertSee('Product');
            $create_visit->loginAs($authUser)
                ->visit('/admin/products/create')
                ->assertSee('Product-Create');
            $product=Product::factory()->create();
            $store_visit->loginAs($authUser)
                ->visitRoute('admin.products.create')
                ->type('name',$product->name)
                ->type('sku',$product->sku)
                ->type('price',$product->price)
                ->press('Сохранить')
                ->assertSee('Фотография');
        });
    }
}
